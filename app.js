var app = require('./src/config/server')
const urls = require('./src/config/url')
require('dotenv').config()
require('./src/config/database')

const port = process.env.PORT || 3030

urls.addURLs(app)

app.listen(port, (err) => {
    if (err) {
        console.log('error: ' + err)
    } else {
        console.log(`Server running on port: ${port}`)
    }
})
